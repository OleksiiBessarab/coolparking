using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Controllers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.IO;
using System.Reflection;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public static IParkingService ParkingService;
        readonly FakeTimerService _withdrawTimer;
        readonly FakeTimerService _logTimer;
        readonly ILogService _logService;


        public Startup(IConfiguration configuration)
        {
            _withdrawTimer = new FakeTimerService();
            _logTimer = new FakeTimerService();
            _logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");

            Startup.ParkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CoolParking.WebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoolParking.WebAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "",
                    pattern: "api/{controller=Parking}/{action=balance}"
                    ); ;
            });
        }
    }
}
