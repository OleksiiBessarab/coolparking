﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Net;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api")]
    public class ParkingController : ControllerBase
    {
        private readonly ILogger<ParkingController> _logger;

        public ParkingController(ILogger<ParkingController> logger)
        {
            _logger = logger;
        }

        [HttpGet("parking/addVehicle")]
        public IEnumerable<Vehicle> Get()
        {
            Startup.ParkingService.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Bus, 100));
            
            return Startup.ParkingService.GetVehicles();
        }

        [HttpGet("parking/balance")]
        public ActionResult<decimal> Balance()
        {
            return Ok(Startup.ParkingService.GetBalance());
        }

        [HttpGet("parking/capacity")]
        public ActionResult<int> Capacity()
        {
            return Ok(Startup.ParkingService.GetCapacity());
        }

        [HttpGet("parking/freePlaces")]
        public ActionResult<int> FreePlaces()
        {
            return Ok(Startup.ParkingService.GetFreePlaces());
        }

        [HttpGet("vehicles")]
        public ActionResult<List<VehicleInfo>> Vehicles()
        {
            List<VehicleInfo> vehicles = new List<VehicleInfo>();
            foreach (var vehicle in Startup.ParkingService.GetVehicles())
            {
                vehicles.Add(new() { Id = vehicle.Id, VehicleType = vehicle.VehicleType, Balance = vehicle.Balance});
            }

            return Ok(JsonConvert.SerializeObject(vehicles));
        }

        [HttpGet("vehicles/{vehicleId?}")]
        public ActionResult<VehicleInfo> Vehicles(string vehicleId)
        {
            if (!Vehicle.IdIsAllowed(vehicleId)) return BadRequest("Vehicle Id is not valid.");

            Vehicle vehicle;
            try
            {
                vehicle = Startup.ParkingService.GetVehicleById(vehicleId);
            }
            catch (Exception except)
            {
                return NotFound(except.ToString());
            }

            return Ok(JsonConvert.SerializeObject(new VehicleInfo() { Id = vehicle.Id, VehicleType = vehicle.VehicleType, Balance = vehicle.Balance}));
        }


        [HttpPost("vehicles")]
        public ActionResult<VehicleInfo> AddVehicle([FromBody] VehicleInfo vehicle)
        {
            try
            {
                Startup.ParkingService.AddVehicle(new Vehicle(vehicle.Id, vehicle.VehicleType, vehicle.Balance));
            }
            catch (Exception except)
            {
                return BadRequest(except.ToString());
            }

            return Ok(new VehicleInfo() { Id = vehicle.Id, Balance = vehicle.Balance, VehicleType = vehicle.VehicleType });
        }

        [HttpDelete("vehicles/{vehicleId}")]
        public ActionResult DeleteVehicle(string vehicleId)
        {
            if (!Vehicle.IdIsAllowed(vehicleId)) return BadRequest("Vehicle Id is not valid.");

            try
            {
                Startup.ParkingService.RemoveVehicle(vehicleId);
            }
            catch (Exception except)
            {
                return NotFound(except.ToString());
            }

            return NoContent();
        }

        [HttpGet("transactions/last")]
        public ActionResult<TransactionInfo> GetLastTransaction()
        {
            var transactions = Startup.ParkingService.GetLastParkingTransactions();

            if (transactions.Length != 0) return Ok(new TransactionInfo(transactions[transactions.Length - 1]));
            else return BadRequest();
        }

        [HttpGet("transactions/all")]
        public ActionResult<string> GetAllTransactions()
        {
            return Ok(Startup.ParkingService.ReadFromLog());
        }

        [HttpPut("transactions/topUpVehicle")]
        public ActionResult<VehicleInfo> TopUpVehicle([FromBody]TopUp topUp)
        {
            Vehicle vehicle;

            if (!Vehicle.IdIsAllowed(topUp.id)) return BadRequest("Vehicle Id is not valid.");
            try
            {
                Startup.ParkingService.TopUpVehicle(topUp.id, topUp.Sum);

                vehicle = Startup.ParkingService.GetVehicleById(topUp.id);
            }
            catch (Exception except)
            {
                return NotFound(except.ToString());
            }

            return Ok(new VehicleInfo() { Id = vehicle.Id, VehicleType = vehicle.VehicleType, Balance = vehicle.Balance});
        }

        public class TransactionInfo
        {
            [JsonProperty("Sum")]
            public decimal Sum;
            [JsonProperty("vehicleId")]
            public string vehicleId;
            [JsonProperty("transactionDate")]
            public DateTime transactionDate;

            public TransactionInfo(CoolParking.BL.Models.TransactionInfo transactionInfo)
            {
                this.Sum = transactionInfo.Sum;
                this.vehicleId = transactionInfo.vehicleId;
                this.transactionDate = transactionInfo.transactionDate;
            }
        }

        public class VehicleInfo
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("vehicleType")]
            public VehicleType VehicleType { get; set; }
            [JsonProperty("balance")]
            public decimal Balance { get; set; }
        }

        public class TopUp
        {
            [JsonProperty("id")]
            public string id { get; set; }
            [JsonProperty("Sum")]
            public decimal Sum { get; set; }
        }
    }
}
