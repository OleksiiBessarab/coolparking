﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static int Balance { get => 0; }
        public static int Capacity { get => 10; }
        public static int PaymentInterval { get => 5; }
        public static int LogInterval { get => 60; }
        public static decimal Penalty { get => 2.5M; }
        public static string LogPath { get => $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log"; }

        public static decimal Tariff(VehicleType vehicleType)
        {
            switch (vehicleType)
            {
                case VehicleType.PassengerCar:
                    return 2;
                case VehicleType.Truck:
                    return 5;
                case VehicleType.Bus:
                    return 3.5m;
                case VehicleType.Motorcycle:
                    return 1;
                default: return 0;
            }
        }
    }
}