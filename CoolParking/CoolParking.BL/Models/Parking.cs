﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;
using System;
using System.Collections.ObjectModel;
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private static Parking instance = new Parking();

        private List<Vehicle> _vehicles = new List<Vehicle>();
        private int _capacity;
        private decimal _balance;
        private List<TransactionInfo> _transactionInfo = new List<TransactionInfo>();
        public ILogService _logService;

        public decimal Balance { get => _balance; }
        public int FreePlaces { get => _capacity - _vehicles.Count; }
        public int Capacity { get => _capacity; }

        private Parking()
        {
            _vehicles = new List<Vehicle>();
            _capacity = Settings.Capacity;
            _balance = Settings.Balance;
        }

        public static Parking Instance
        {
            get
            {
                return instance;
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionInfo.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }
        public void Withdraw(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var vehicle in _vehicles)
            {
                decimal sum = 0;

                if (vehicle.Balance < Settings.Tariff(vehicle.VehicleType))
                {
                    ReduseVehicle(vehicle.Id, vehicle.Balance);

                    decimal penalty = (Settings.Tariff(vehicle.VehicleType) - vehicle.Balance) * Settings.Penalty;

                    ReduseVehicle(vehicle.Id, penalty);

                    sum = vehicle.Balance + penalty;
                }
                else
                {
                    ReduseVehicle(vehicle.Id, Settings.Tariff(vehicle.VehicleType));
                    sum = Settings.Tariff(vehicle.VehicleType);
                }

                AddBalance(sum);
                _transactionInfo.Add(new() { Sum = sum, vehicleId = vehicle.Id, transactionDate = DateTime.Now });
            }
        }

        public void LogInvoke(object sender, ElapsedEventArgs e)
        {
            foreach (var transactionInfo in _transactionInfo)
            {
                _logService.Write($"DateTime: {transactionInfo.transactionDate}; PlateNumber: {transactionInfo.vehicleId}; Sum: {transactionInfo.Sum}");
            }
            _transactionInfo = new List<TransactionInfo>();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_vehicles.Count >= _capacity) throw new InvalidOperationException();
            if (_vehicles.Find(x => x.Id == vehicle.Id) != null) throw new ArgumentException();

            _vehicles.Add(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal addBalance)
        {
            if (addBalance < 0) throw new ArgumentException();
            Vehicle vehicle = GetVehicleById(vehicleId);

            vehicle.Balance += addBalance;
        }

        public void ReduseVehicle(string vehicleId, decimal reduseBalance)
        { 
            Vehicle vehicle = GetVehicleById(vehicleId);

            vehicle.Balance -= reduseBalance;
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (GetVehicleById(vehicleId).Balance < 0) throw new InvalidOperationException();

            _vehicles.Remove(GetVehicleById(vehicleId));
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_vehicles);
        }

        public Vehicle GetVehicleById(string vehicleId)
        {
            Vehicle vehicle = _vehicles.Find(c => c.Id == vehicleId);

            if (vehicle == null) throw new ArgumentException($"There is no vehicle with {vehicleId} id");

            return vehicle;
        }

        public void AddBalance (decimal addBalance)
        {
            _balance += addBalance;
        }

        public void Dispose()
        {
            instance = new Parking();
        }
    }
}