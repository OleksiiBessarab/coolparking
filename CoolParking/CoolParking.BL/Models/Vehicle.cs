﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public readonly string Id;
        public decimal Balance { get; set; }
        public readonly VehicleType VehicleType;
        
        public Vehicle(string vehicleId, VehicleType vehicleType, decimal balance)
        {
            if (!IdIsAllowed(vehicleId) || balance <= 0) throw new ArgumentException();

            Id = vehicleId;
            VehicleType = vehicleType;
            this.Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var plateNumber = $"{chars[random.Next(chars.Length)]}{chars[random.Next(chars.Length)]}-{random.Next(9)}{random.Next(9)}{random.Next(9)}{random.Next(9)}-{chars[random.Next(chars.Length)]}{chars[random.Next(chars.Length)]}";

            return plateNumber;
        }

        public static bool IdIsAllowed(string id)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string numbers = "0123456789";

            if (id.Length != 10) return false;

            for (int i = 0; i < 10; i++)
            {
                var letter = id.Substring(i, 1);
                switch (i)
                {
                    case 0:
                    case 1:
                    case 8:
                    case 9:
                        if (!chars.Contains(letter)) return false;
                        break;
                    case 2:
                    case 7:
                        if (letter != "-") return false;
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        if (!numbers.Contains(letter)) return false;
                        break;
                }
            }

            return true;
        }
    }
}