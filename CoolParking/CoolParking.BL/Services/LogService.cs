﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;

namespace CoolParking.BL.Services
{
    public class LogService : Interfaces.ILogService
    {
        private readonly string _logPath;
        public string LogPath => _logPath;

        public LogService(string logPath)
        {
            _logPath = logPath;
        }

        public string Read()
        {
            try
            {
                return System.IO.File.ReadAllText(_logPath);
            }
            catch (System.Exception)
            {

                throw new InvalidOperationException();
            }
        }

        public void Write(string logInfo)
        {
            try
            {
                using System.IO.StreamWriter file = new(_logPath, append: true);
                file.WriteLineAsync(logInfo);
            }
            catch (Exception)
            {

                throw new InvalidOperationException();
            }
        }
    }

}