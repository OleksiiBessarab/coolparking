﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Timers;

namespace CoolParking.BL.Services 
{
    public class ParkingService : IDisposable, IParkingService
    {
        private bool disposedValue;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            withdrawTimer.Elapsed += Withdraw;
            logTimer.Elapsed += LogInvoke;
            Parking.Instance._logService = logService;
        }

        private void LogInvoke(object sender, ElapsedEventArgs e)
        {
            Parking.Instance.LogInvoke(sender, e);
        }

        private void Withdraw(object sender, System.Timers.ElapsedEventArgs e)
        {
            Parking.Instance.Withdraw(sender, e);
        }

        public void TopUpVehicle(string vehicleId, decimal addBalance)
        {
            Parking.Instance.TopUpVehicle(vehicleId, addBalance);
        }
        
        public void AddVehicle(Vehicle vehicle)
        {
            Parking.Instance.AddVehicle(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            Parking.Instance.RemoveVehicle(vehicleId);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Parking.Instance.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public decimal GetBalance()
        {
            return Parking.Instance.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Instance.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Instance.FreePlaces;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.Instance.GetLastParkingTransactions();
        }

        public string ReadFromLog()
        {
            return Parking.Instance.ReadFromLog();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.Instance.GetVehicles();
        }

        public Vehicle GetVehicleById(string vehicleId)
        {
            return Parking.Instance.GetVehicleById(vehicleId);
        }

        public bool VehicleIdIsValid(string vehicleId)
        {
            return Vehicle.IdIsAllowed(vehicleId);
        }
    }
}